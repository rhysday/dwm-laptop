/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int gappih    = 1;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 4;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 20;       /* vert outer gap between windows and screen edge */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int smartgaps          = 1;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Fira Code Retina:size=8.9", "NotoColorEmoji:pixelsize=12", "FontAwesome:pixelsize=15:antialias=true:autohint=true" };
static char dmenufont[]             = "Fira Code Retina:size=9";

static char normbgcolor[]           = "#21242b";
static char normbordercolor[]       = "#1d2026";
static char normfgcolor[]           = "#9ca0a4";
static char selfgcolor[]            = "#dfdfdf";

static char barcolor[]              = "#3f444a";
static char bartxtcolor[]           = "#bfbfbf";
static char tagtxtcolor[]           = "#51afef";
static char tagselcolor[]           = "#8883d8";
/* static char tagselcolor[]           = "#8883d8"; */
	/* 7b76cb */
static char selbordercolor[]        = "#51afef";
static char selbgcolor[]            = "#51afef";

static char *colors[][3] = {
       /*                   fg               bg             border   */
       [SchemeNorm]     = { normfgcolor,     normbgcolor,   normbordercolor },
       [SchemeSel]      = { selfgcolor,      selbgcolor,    selbordercolor  },
	   [SchemeStatus]   = { normfgcolor,     normbgcolor,    "#000000"       }, // Statusbar right {text,background,not used but cannot be empty}
       [SchemeTagsSel]  = { normbgcolor,     tagselcolor,   "#000000"       }, // Tagbar left selected {text,background,not used but cannot be empty}
       [SchemeTagsNorm] = { tagtxtcolor,     normbgcolor,  "#000000"       }, // Tagbar left unselected {text,background,not used but cannot be empty}
       [SchemeInfoSel]  = { bartxtcolor,     barcolor,      "#000000"       }, // infobar middle  selected {text,background,not used but cannot be empty}
       [SchemeInfoNorm] = { normfgcolor,     normbgcolor,   "#000000"       }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {"st", "-n", "spterm", "-g", "120x34", NULL };
const char *spcmd2[] = {"st", "-n", "spcalc", "-f", "monospace:size=16", "-g", "50x20", "-e", "bc", "-lq", NULL };
const char *spcmd3[] = {"st", "-n", "sphtop", "-g", "120x34", "-e", "htop", NULL };
const char *spcmd4[] = {"st", "-n", "sptremc", "-g", "120x34", "-e", "tremc", NULL };
const char *spcmd5[] = {"st", "-n", "spncpamixer", "-g", "120x34", "-e", "ncpamixer", NULL };
static Sp scratchpads[] = {
	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spcalc",      spcmd2},
	{"sphtop",      spcmd3},
	{"sptremc",     spcmd4},
	{"sptremc",     spcmd5},
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	*/
	/* class             instance      title       	 tags mask    iscentered  isfloating   isterminal  noswallow  monitor */
	{ "St",              NULL,       NULL,       	   0,            0,           0,           1,         0,        -1 },
	{ NULL,              NULL,       "Event Tester",   0,            0,           0,           0,         1,        -1 },
	{ NULL,              "spterm",   NULL,       	   SPTAG(0),     0,           1,           1,         0,        -1 },
	{ NULL,              "spcalc",   NULL,       	   SPTAG(1),     0,           1,           1,         0,        -1 },
	{ NULL,              "sphtop",   NULL,       	   SPTAG(2),     0,           1,           1,         0,        -1 },
	{ NULL,              "sptremc",  NULL,       	   SPTAG(3),     0,           1,           1,         0,        -1 },
	{ NULL,              "spncpamixer",  NULL,         SPTAG(4),     0,           1,           1,         0,        -1 },
	{ "Alacritty",       NULL,       NULL,             0,            0,           0,           1,         0,        -1 },
	{ "Gtkcord3",        NULL,       NULL,             1 << 0,       0,           0 },
	{ "qutebrowser",     NULL,       NULL,             0,            0,           0 },
	{ "Tor Browser",     NULL,       NULL,             0,            0,           0 },
	{ "Virt-manager",          NULL,       NULL,             1 << 1,       0,           0 },
	{ "REAPER",          NULL,       NULL,             1 << 2,       0,           0 },
	{ "resolve",         NULL,       NULL,             1 << 3,       0,           0 },
	{ "Fusion",          NULL,       NULL,             1 << 4,       0,           0 },
	{ "krita",           NULL,       NULL,             1 << 5,       0,           0 },
	{ "Inkscape",        NULL,       NULL,             1 << 5,       0,           0 },
	{ "Gimp",            NULL,       NULL,             1 << 5,       0,           0 },
	{ "Blender",         NULL,       NULL,             1 << 5,       0,           0 },
	{ "mpv",             NULL,       NULL,             0,            0,           0 },
	{ "LibreWolf",       NULL,       NULL,             0,            0,           0 },
	{ "FreeTube",        NULL,       NULL,             1 << 6,       0,           0 },
	{ "LBRY",            NULL,       NULL,             1 << 6,       0,           0 },
	{ "Steam",           NULL,       NULL,             1 << 7,       0,           0 },
	{ "SC Controller",   NULL,       NULL,             1 << 8,       0,           0 },
	{ "Non-Mixer",       NULL,       NULL,             1 << 8,       0,           0 },
	{ "Carla2",          NULL,       NULL,             1 << 8,       0,           0 },
	{ "QjackCtl",        NULL,       NULL,             9,            0,           1 },
	{ "wootility",       NULL,       NULL,             0,            0,           1 },
	{ "KeePassXC",       NULL,       NULL,             0,            0,           1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[\\]",	dwindle },		/* Decreasing in size right and leftward */
	{ "[@]",	spiral },		/* Fibonacci spiral */

	{ "TTT",	bstack },		/* Master on top, slaves on bottom */
 	{ "[]=",	tile },			/* Default: Master on left, slaves on right */

	{ "H[]",	deck },			/* Master on left, slaves in monocle-like mode on right */
 	{ "[M]",	monocle },		/* All windows on top of eachother */

	{ "|M|",	centeredmaster },		/* Master in middle, slaves on sides */
	{ ">M>",	centeredfloatingmaster },	/* Same but master floats */

	{ "><>",	NULL },			/* no layout function means floating behavior */
	{ NULL,		NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD,	XK_j,	ACTION##stack,	{.i = INC(+1) } }, \
	{ MOD,	XK_k,	ACTION##stack,	{.i = INC(-1) } }, \
	{ MOD,  XK_v,   ACTION##stack,  {.i = 0 } }, \
	/* { MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \ */
	/* { MOD, XK_a,     ACTION##stack, {.i = 1 } }, \ */
	/* { MOD, XK_z,     ACTION##stack, {.i = 2 } }, \ */
	/* { MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", tagtxtcolor, "-sb", tagselcolor, "-sf", normbordercolor, NULL };
static const char *termcmd[]  = { "alacritty", NULL };

#include <X11/XF86keysym.h>
#include "shiftview.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	STACKKEYS(MODKEY,                          focus)
	STACKKEYS(MODKEY|ShiftMask,                push)
	/* { MODKEY|ShiftMask,		XK_Escape,	spawn,	SHCMD("") }, */
	{ MODKEY,			XK_grave,	spawn,	SHCMD("dmenuunicode") },
	/* { MODKEY|ShiftMask,		XK_grave,	togglescratch,	SHCMD("") }, */
	TAGKEYS(			XK_1,		0)
	TAGKEYS(			XK_2,		1)
	TAGKEYS(			XK_3,		2)
	TAGKEYS(			XK_4,		3)
	TAGKEYS(			XK_5,		4)
	TAGKEYS(			XK_6,		5)
	TAGKEYS(			XK_7,		6)
	TAGKEYS(			XK_8,		7)
	TAGKEYS(			XK_9,		8)
	{ MODKEY,			XK_0,		view,		{.ui = ~0 } },
	{ MODKEY|ShiftMask,	XK_0,		tag,		{.ui = ~0 } },
/*	{ MODKEY,			XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") }, */
/*	{ MODKEY|ShiftMask,		XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") }, */
/*	{ MODKEY,			XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") }, */
/*	{ MODKEY|ShiftMask,		XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") }, */
	{ MODKEY,			XK_BackSpace,	spawn,		SHCMD("sysact") },
	/*{ MODKEY|ShiftMask,		XK_BackSpace,	spawn,		SHCMD("") }, */

	{ MODKEY,			XK_Tab,		view,		{0} },
	/*{ MODKEY|ShiftMask,		XK_Tab,		spawn,		SHCMD("") }, */
	{ MODKEY,			XK_q,		killclient,	{0} },
	{ MODKEY,			XK_w,		spawn,		SHCMD("qutebrowser-wrap") },
	{ MODKEY,    	    XK_e,		spawn,		SHCMD("emacsclient -a '' -nc -e '(mu4e)' ; pkill -RTMIN+12 dwmblocks") },
	{ MODKEY|ShiftMask,	XK_e,		spawn,		SHCMD("emacsclient -a '' -nc -e '(elfeed)'") },
	{ MODKEY,			XK_d,		spawn,		SHCMD("emacsclient -nc -a '' '~/'") },
	{ MODKEY,	        XK_a,		spawn,		SHCMD("emacsclient -a '' -c -e '(org-agenda)'") },
	{ MODKEY,	        XK_n,		spawn,		SHCMD("emacsclient -a '' -c ~/org/notes.gpg") },
	{ MODKEY|ShiftMask,	XK_n,		spawn,		SHCMD("emacsclient -a '' -c ~/org/gtd.gpg") },
	{ MODKEY,			XK_r,		spawn,		SHCMD("thunar") },
	{ MODKEY,	        XK_s,		spawn,		SHCMD("steam-wrap") },
	{ MODKEY,	        XK_z,		spawn,		SHCMD("sccontroller-wrap") },
	{ MODKEY|ShiftMask, XK_w,		spawn,		SHCMD("librewolf-wrap") },
	{ MODKEY|ShiftMask, XK_d,		spawn,		SHCMD("discord-wrap") },
	{ MODKEY|ShiftMask, XK_q,		spawn,		SHCMD("tor-wrap") },
	{ MODKEY|ShiftMask,	XK_r,		spawn,		SHCMD("reaper-wrap") },
	{ MODKEY|ShiftMask,	XK_b,		spawn,		SHCMD("blender") },
	/*{ MODKEY|ShiftMask,	XK_x,		spawn,		SHCMD("alacritty -e tremc") },*/
	{ MODKEY|ShiftMask,	XK_p,		spawn,		SHCMD("rofi-pass") },
	{ MODKEY,			XK_t,		setlayout,	{.v = &layouts[0]} },
	{ MODKEY|ShiftMask,	XK_t,		setlayout,	{.v = &layouts[1]} },
	{ MODKEY,			XK_y,		setlayout,	{.v = &layouts[2]} },
	{ MODKEY|ShiftMask,	XK_y,		setlayout,	{.v = &layouts[3]} },
	{ MODKEY,			XK_u,		setlayout,	{.v = &layouts[4]} },
	{ MODKEY|ShiftMask,	XK_u,		setlayout,	{.v = &layouts[5]} },
	{ MODKEY,			XK_i,		setlayout,	{.v = &layouts[6]} },
	{ MODKEY|ShiftMask,	XK_i,		setlayout,	{.v = &layouts[7]} },
	{ MODKEY,			XK_o,		incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,	XK_o,		incnmaster,     {.i = -1 } },
	/* { MODKEY,	XK_End,			spawn,		SHCMD("mpc toggle") }, */
	{ 0,	            XK_Pause,			spawn,		SHCMD("mpc stop ; stopallmpv") },
	{ MODKEY,			XK_bracketleft,		spawn,		SHCMD("mpc seek -10") },
	{ MODKEY|ShiftMask,	XK_bracketleft,		spawn,		SHCMD("mpc seek -120") },

	{ MODKEY,			XK_bracketright,	spawn,		SHCMD("mpc seek +10") },
	{ MODKEY|ShiftMask,	XK_bracketright,	spawn,		SHCMD("mpc seek +120") },
	{ MODKEY,			XK_backslash,		view,		{0} },
	/* { MODKEY|ShiftMask,		XK_backslash,		spawn,		SHCMD("") }, */
	/*{ MODKEY|ShiftMask,	XK_a,		spawn,		SHCMD("alacritty -e startjack") },*/
	/* { MODKEY|ShiftMask,		XK_a,		spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		XK_s,		spawn,		SHCMD("") }, */
	{ MODKEY|ShiftMask,	XK_Return,	spawn,          {.v = dmenucmd } },
	{ MODKEY,			XK_f,		togglefullscr,	{0} },
	{ MODKEY|ShiftMask,	XK_f,		setlayout,	{.v = &layouts[8]} },
	{ MODKEY,			XK_g,		shiftview,	{ .i = -1 } },
	/* { MODKEY|ShiftMask,		    XK_g,		spawn,		SHCMD("") }, */
	{ MODKEY,			XK_h,		setmfact,	{.f = -0.05} },
	/* J and K are automatically bound above in STACKEYS */
	{ MODKEY,			XK_l,		setmfact,      	{.f = +0.05} },
	{ MODKEY,			XK_semicolon,	shiftview,	{ .i = 1 } },
	/* { MODKEY|ShiftMask,		    XK_semicolon,	shiftview,	SHCMD("") }, */
	/* { MODKEY,			        XK_apostrophe,	spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,		    XK_apostrophe,	spawn,		SHCMD("") }, */
	{ MODKEY,			XK_Return,	spawn,		{.v = termcmd } },

	{ MODKEY|ShiftMask,	XK_s,	    togglescratch, {.ui = 0} },
	{ MODKEY,			XK_c,	    togglescratch, {.ui = 1} },
	{ MODKEY|ShiftMask,	XK_Escape,	togglescratch, {.ui = 2} },
	{ MODKEY,       	XK_x,	    togglescratch, {.ui = 3} },

	{ MODKEY|ShiftMask,	XK_z,		togglegaps,	{0} },
	{ MODKEY|ShiftMask,	XK_x,		incrgaps,	{.i = +3 } },
	{ MODKEY|ShiftMask,	XK_c,		incrgaps,	{.i = -3 } },
	{ MODKEY|ShiftMask,	XK_v,		spawn,		SHCMD("{ killall picom || setsid picom --experimental-backends & }") },
	{ MODKEY|ShiftMask,	XK_a,	    spawn,      SHCMD("alacritty -e feh --bg-fill --randomize --no-fehbg ~/.local/bin/wallpapers/* & ") },
	{ MODKEY,			XK_b,		togglebar,	{0} },
	/*{ MODKEY,			XK_x,		spawn,		SHCMD("") }, */
	/*{ MODKEY|ShiftMask,	XK_c,		spawn,		SHCMD("mpv --no-cache --no-osc --no-input-default-bindings --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)") },*/
	/*{ MODKEY,			XK_v,		spawn,		SHCMD("alacritty -e $EDITOR -c \"VimwikiIndex\"") },*/
	/* { MODKEY|ShiftMask,		XK_b,		spawn,		SHCMD("") }, */
	/*{ MODKEY,			XK_n,		spawn,		SHCMD("alacritty -e newsboat; pkill -RTMIN+6 dwmblocks") },*/
	/* { MODKEY|ShiftMask,		XK_n,		spawn,		SHCMD("") }, */
	{ MODKEY,			XK_m,		spawn,		SHCMD("alacritty -e ncmpcpp") },
	{ MODKEY|ShiftMask,	XK_m,		spawn,		SHCMD("alacritty -e cava") },
	{ MODKEY,			XK_comma,	spawn,		SHCMD("mpc prev") },
	{ MODKEY|ShiftMask,	XK_comma,	spawn,		SHCMD("mpc seek 0%") },
	{ MODKEY,			XK_period,	spawn,		SHCMD("mpc next") },
	{ MODKEY|ShiftMask,	XK_period,	spawn,		SHCMD("mpc repeat") },

	{ MODKEY,			XK_Left,	focusmon,	{.i = -1 } },
	{ MODKEY|ShiftMask,	XK_Left,	tagmon,		{.i = -1 } },
	{ MODKEY,			XK_Right,	focusmon,	{.i = +1 } },
	{ MODKEY|ShiftMask,	XK_Right,	tagmon,		{.i = +1 } },

	{ MODKEY,			XK_Page_Up,	shiftview,	{ .i = -1 } },
	{ MODKEY,			XK_Page_Down,	shiftview,	{ .i = 1 } },
	{ MODKEY,			XK_Insert,	spawn,		SHCMD("notify-send \"📋 Clipboard contents:\" \"$(xclip -o -selection clipboard)\"") },

	/*{ MODKEY,			XK_F3,		spawn,		SHCMD("displayselect") },*/
	{ MODKEY,			XK_F1,		spawn,		SHCMD("groff -mom ~/.local/share/larbs/readme.mom -Tpdf | zathura -") },
	{ MODKEY,	        XK_F2,		spawn,		SHCMD("tutorialvids") },
	{ MODKEY,			XK_F3,		quit,		{1} },
	{ MODKEY,			XK_F4,		spawn,		SHCMD("stalonetray") },
	/*{ MODKEY,			XK_F4,		spawn,		SHCMD("mbsync -c /home/me/.config/isync/mbsyncrc mailbox") },*/
	{ MODKEY,			XK_F5,		spawn,		SHCMD("systemctl stop vpn") },
	{ MODKEY,			XK_F6,		spawn,		SHCMD("systemctl start vpn") },
	{ MODKEY,			XK_F7,		spawn,		SHCMD("dmenuumount") },
	{ MODKEY,			XK_F8,		spawn,		SHCMD("dmenumount") },
	{ MODKEY,			XK_F9,		spawn,		SHCMD("resolve-wrap") },
	{ MODKEY,			XK_F10,		spawn,		SHCMD("fusion-wrap") },
	{ MODKEY,			XK_F11,		spawn,		SHCMD("alacritty -e sudo win10") },
	/* { MODKEY,			XK_F11,		xrdb,		{.v = NULL } }, */
	{ MODKEY,			XK_F12,		spawn,		SHCMD("alacritty -e ytmpv") },

	{ MODKEY,			XK_space,	zoom,		{0} },
	{ MODKEY|ShiftMask,	XK_space,	togglefloating,	{0} },

	{ 0,				XK_Print,	spawn,		SHCMD("flameshot gui") },
	/* { 0,				XK_Print,	spawn,		SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") }, */
	{ ShiftMask,		XK_Print,	spawn,		SHCMD("maimpick") },
	{ MODKEY,			XK_Print,	spawn,		SHCMD("dmenurecord") },
	{ MODKEY|ShiftMask,	XK_Print,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY,			XK_Delete,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY,			XK_Scroll_Lock,	spawn,	SHCMD("killall screenkey || screenkey &") },

	/*{ 0, XF86XK_AudioMute,		    spawn,      SHCMD("telegram-desktop") },
	{ 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("lbry-wrap") },*/
	{ 0, XF86XK_AudioRaiseVolume,	togglescratch, {.ui = 4} }
	{ 0, XF86XK_AudioMute,		spawn,		SHCMD("ncpamixer -t; kill -44 $(pidof dwmblocks)") },
	/*{ 0, XF86XK_AudioRaiseVolume,	spawn,		SHCMD("ncpamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)") },*/
	{ 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("ncpamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioPrev,		    spawn,		SHCMD("mpc prev") },
	{ 0, XF86XK_AudioNext,		    spawn,		SHCMD("mpc next") },
	/*{ 0, XF86XK_AudioPause,		spawn,		SHCMD("mpc stop ; stopallmpv") },*/
	{ 0, XF86XK_AudioPlay,		    spawn,		SHCMD("mpc toggle") },
	/*{ 0, XF86XK_AudioPlay,		spawn,		SHCMD("mpc play") },*/
	{ 0, XF86XK_AudioStop,		    spawn,		SHCMD("mpc stop") },
	{ 0, XF86XK_AudioRewind,	    spawn,		SHCMD("mpc seek -10") },
	{ 0, XF86XK_AudioForward,	    spawn,		SHCMD("mpc seek +10") },
	{ 0, XF86XK_AudioMedia,		    spawn,		SHCMD("alacritty -e ncmpcpp") },
	{ 0, XF86XK_PowerOff,		    spawn,		SHCMD("[ \"$(printf \"No\\nYes\" | dmenu -i -nb darkred -sb red -sf white -nf gray -p \"Suspend computer?\")\" = Yes ] && systemctl suspend") },
	{ 0, XF86XK_Calculator,		    spawn,		SHCMD("alacritty -e bc -l") },
	{ 0, XF86XK_Sleep,		        spawn,		SHCMD("sudo -A zzz") },
	{ 0, XF86XK_ScreenSaver,	    spawn,		SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_Launch1,		    spawn,		SHCMD("xset dpms force off") },
};

	/*{ MODKEY,			XK_n,		spawn,		SHCMD("st -e nvim -c VimwikiIndex") },
	{ MODKEY|ShiftMask,	XK_n,	    spawn,		SHCMD("st -e newsboat; pkill -RTMIN+6 dwmblocks") },
	{ MODKEY,			XK_m,		spawn,		SHCMD("st -e ncmpcpp") },
	{ MODKEY|ShiftMask,	XK_m,		spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,			XK_comma,	spawn,		SHCMD("mpc prev") },
	{ MODKEY|ShiftMask,	XK_comma,	spawn,		SHCMD("mpc seek 0%") },
	{ MODKEY,			XK_period,	spawn,		SHCMD("mpc next") },
	{ MODKEY|ShiftMask,	XK_period,	spawn,		SHCMD("mpc repeat") }, */

	/* { 0, XF86XK_ScreenSaver,	spawn,		SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_Mail,		spawn,		SHCMD("st -e neomutt ; pkill -RTMIN+12 dwmblocks") }, */

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
	{ ClkStatusText,        ShiftMask,      Button3,        spawn,          SHCMD("emacsclient -a '' -c ~/.local/github/dwmblocks/dwmblocks/config.h") },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        defaultgaps,	{0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkClientWin,		MODKEY,		Button4,	incrgaps,	{.i = +1} },
	{ ClkClientWin,		MODKEY,		Button5,	incrgaps,	{.i = -1} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkTagBar,		0,		Button4,	shiftview,	{.i = -1} },
	{ ClkTagBar,		0,		Button5,	shiftview,	{.i = 1} },
	{ ClkRootWin,		0,		Button2,	togglebar,	{0} },
};
